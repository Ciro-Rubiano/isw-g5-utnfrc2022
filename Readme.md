### Definición de Lineas Base

- 1º Linea base - Estado del repositorio previo a el primer parcial de la materia - (17 septiembre aprox)

- 2º Linea base - Estado del repositorio previo a el segundo parcial de la materia - (29 octubre aprox)

- 3º Linea base - Estado del repositorio previo al recuperatorio de la materia - (19 noviembre)

# Estructura de Repositorio

```
╚═ Proyecto ISW - 2022 - Grupo 5
    ╠═ Cursado
    ║    ╠═ Clases
    ║    ╠═ Parciales
    ║    ║    ╠═ Templates 
    ║    ║    ╠═ 1er Parcial
    ║    ║    ║    ╠═ Ejercitación
    ║    ║    ║    ╚═ Enunciados
    ║    ║    ╠═ 2do Parcial
    ║    ║    ║    ╠═ Ejercitación
    ║    ║    ║    ╚═ Enunciados
    ║    ║    ╚═ Recuperatorio
    ║    ║         ╠═ Ejercitación
    ║    ║         ╚═ Enunciados
    ║    ╠═ Trabajos Conceptuales
    ║    ║    ╠═ Trabajo Conceptual 1
    ║    ║    ╚═ Trabajo Conceptual 2
    ║    ╚═ Trabajos Prácticos
    ║              ╠═ Resoluciones Evaluables
    ║              ║    ╠═ TP 04
    ║              ║    ╠═ TP 05
    ║              ║    ╠═ TP 06
    ║              ║    ║    ╚═ US - Realizar Pedido de lo que sea
    ║              ║    ╠═ TP 07
    ║              ║    ╠═ TP 08
    ║              ║    ╠═ TP 12
    ║              ║    ╠═ TP 13
    ║              ║    ╚═ TP 14
    ║              ╚═ Resoluciones No Evaluables
    ║                  ╠═ TP 01
    ║                  ╠═ TP 02
    ║                  ╠═ TP 03
    ║                  ╠═ TP 09
    ║                  ╠═ TP 10
    ║                  ╚═ TP 11
    ╠═ Material de Estudio
    ║    ╠═ Bibliografia
    ║    ║   ╠═ Libros
    ║    ║   ╠═ Papers
    ║    ║   ╚═ Otros
    ║    ╠═ Practico
    ║    ║   ╠═ Trabajos Practicos Resueltos
    ║    ║   ╚═ Otros
    ║    ╚═ Teorico
    ║         ╚═ Filminas y presentaciones
    ╠═ Reglas de juego
    ╚═ Sin Clasificar
```

# Ítems de Configuración

| Nombre de Ítem de Configuración                  | Regla de Nombrado                                                           | Ubicación Física                                                |
| ------------------------------------------------ | --------------------------------------------------------------------------- | --------------------------------------------------------------- |
| Programa de la materia                           | ISW_Programa_2022.pdf                                                       | /Reglas de Juego/                                               |
| Bibliografia recomendada                         | ISW_Bibliografia_Recomendada_2022.pdf                                       | /Reglas de Juego/                                               |
| Material de Apoyo para Parciales                 | ISW_Material_De_Apoyo_Para_Parciales.pdf                                    | /Reglas de Juego/                                               |
| Libros                                           | ISW_«Título»_«Autor».«extensión»                                            | /Material de Estudio/Bibliografia/Libros/                       |
| Sitios Web                                       | ISW_Sitios_Web.txt                                                          | /Material de Estudio/Bibliografia/                              |
| Filminas de Clase                                | ISW_«U-N»_«Tema».pdf                                                        | /Material de estudio/Teorico/Filminas y Presentaciones/         |
| Templates para rendir parciales                  | ISW_Parcial_«Nro Parcial»_Template.docx                                     | /Cursado/Parciales/Templates                                    |
| Trabajos Prácticos Resueltos                     | ISW_Trabajos_Practicos_Resueltos_2022.pdf                                   | /Material de estudio/Practico/Trabajos Practicos Resueltos/     |
| Otros - Material Práctico Extra                  | ISW_Material_Extra_«Nro»_«Tema».«extensión»                                 | /Material de estudio/Practico/Otros/                            |
| Enunciados Trabajos Conceptuales                 | ISW_Lineamiento_Trabajos_Teoricos_2022.docx                                 | /Cursado/Trabajos Conceptuales/Enunciados/                      |
| Trabajo Conceptual desarrollado                  | ISW_Trabajo_Conceptual_«Nro»_Grupo5_2022_4K2.<<extensión>>                         | /Cursado/Trabajos Conceptuales/Trabajo Conceptual «Nro»/        |
| Enunciados Trabajos Prácticos                    | ISW_Trabajos_Practicos_2022.pdf                                             | /Cursado/Trabajos Practicos                                     |
| Resoluciones de Trabajos Prácticos No Evaluables | ISW_Resolucion_Trabajo_Practico_NoEvaluable_«Nro»_Grupo5_2022_4K2.docx      | /Cursado/Trabajo Practicos/Resoluciones No Evaluables/TP «Nro»/ |
| Resoluciones de Trabajos Prácticos Evaluables    | ISW_Resolucion_Trabajo_Practico_Evaluable_«Nro»_Grupo5_2022_4K2.«extensión» | /Cursado/Trabajo Practicos/Resoluciones Evaluables/TP «Nro»/    |
| Enunciados Parciales                             | ISW_Parcial_Tema_«Nro»_«Nom_Enunc»_«T o P»«Año».«extensión»                 | /Cursado/Parciales/«Nro Parcial»/Enunciado/                     |
| Ejercitaciones Parciales Prácticos               | ISW_Ejercitacion_Parcial_«Nom_Ejercicio».«extensión»                        | /Cursado/Parciales/«Nro Parcial»/Ejercitacion/                  |
| Resúmenes Parciales Teóricos                     | ISW_Resumen_Parcial_«Nro»_«Apellido».«extensión»                            | /Cursado/Parciales/«Nro Parcial»/Ejercitacion/                  |
| Links de Clases                                  | ISW_Links_Clases_Pasadas.xlsx                                               | /Cursado/Clases/                                                |
| Sin Clasificación                                | «Nombre».«extensión»                                                        | /Sin Clasificar/                                                |

## Glosario

| Sigla           | Significado                                                                                                                                                        |
| --------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| «U-N»           | Número de Unidad                                                                                                                                                   |
| «Tema»          | Nombre del Tema                                                                                                                                                    |
| «Título»        | Nombre de un libro                                                                                                                                                 |
| «Autor»         | Apellido del autor del libro                                                                                                                                       |
| «extensión»     | Formato del archivo                                                                                                                                                |
| «T o P»         | Teórico o Práctico                                                                                                                                                 |
| «Apellido»      | Quien escribio el archivo                                                                                                                                          |
| «Nro»           | Número de secuencia para archivos o directorios, según el artefacto que corresponda. En caso de tratarse del parcial recuperatorio, podrá reemplazarse por una "R" |
| «Nro Parcial»   | Es reemplazable por “1er Parcial”, “2do Parcial” ó “Recuperatorio”                                                                                                 |
| «Nombre»        | Nombre del archivo                                                                                                                                                 |
| «Nom_Ejercicio» | Nombre del ejercicio                                                                                                                                               |
| «Nom_Enunc»     | Nombre del Enunciado del ejercicio / dominio                                                                                                                       |
| «Año»           | Año referente del archivo                                                                                                                                          |
