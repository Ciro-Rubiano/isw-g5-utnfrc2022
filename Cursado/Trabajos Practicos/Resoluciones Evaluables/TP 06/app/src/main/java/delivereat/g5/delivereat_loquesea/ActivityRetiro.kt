package delivereat.g5.delivereat_loquesea

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.widget.*
import androidx.appcompat.app.AppCompatActivity


class ActivityRetiro : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_retiro)
        setTitle("Datos del lugar de retiro 🏪")

        val modelo : DeliverEatModel = intent.extras!!.get("model") as DeliverEatModel


        val spinnerCiudades = findViewById<Spinner>(R.id.spinner_ciudades)
        val txtNombreCalle = findViewById<TextView>(R.id.txt_nombre_calle)
        val txtNroCalle = findViewById<TextView>(R.id.txt_nro_calle)
        val btnConfirmar = findViewById<Button>(R.id.button_confirmar_comercio)

        val items = arrayOf("Córdoba", "Villa Allende", "Villa Carlos Paz", "San Francisco")
        val adapter: ArrayAdapter<String> =
            ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, items)
        spinnerCiudades.adapter = adapter;


        txtNroCalle.setInputType(InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED)




        btnConfirmar.setOnClickListener(){



            var okAll = !txtNroCalle.text.toString().isNullOrEmpty() && !txtNombreCalle.text.toString().isNullOrEmpty()

            if(!okAll){
                Toast.makeText(this, "❌ Por favor completar todos los campos", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            modelo.direccion_retiro_numero_calle = txtNroCalle.text.toString()
            modelo.direccion_retiro_ciudad = spinnerCiudades.selectedItem.toString()
            modelo.direccion_retiro_nombre_calle = txtNombreCalle.text.toString()

            val data = Intent()
            data.putExtra("model",modelo)
            setResult(RESULT_OK, data)
            finish()
        }


    }
}