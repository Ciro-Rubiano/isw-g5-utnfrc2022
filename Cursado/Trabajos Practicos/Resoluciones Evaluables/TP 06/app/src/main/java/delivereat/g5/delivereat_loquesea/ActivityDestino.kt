package delivereat.g5.delivereat_loquesea

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import java.util.*

class ActivityDestino : AppCompatActivity() {
    val fechaSeleccionadaEnVista: Calendar = Calendar.getInstance()
    val modelo: DeliverEatModel =
        DeliverEatModel()
    val horas = arrayOf(
        "8 hs",
        "9 hs",
        "10 hs",
        "11 hs",
        "12 hs",
        "13 hs",
        "14 hs",
        "15 hs",
        "16 hs",
        "17 hs",
        "18 hs",
        "19 hs",
        "20 hs",
        "21 hs",
        "22 hs",
        "23 hs"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destino)
        title = "Datos del Destino \uD83C\uDFE0"

        val modelo : DeliverEatModel = intent.extras!!.get("model") as DeliverEatModel

        val spinnerCiudades = findViewById<Spinner>(R.id.spinner_ciudades)
        val txtNombreCalle = findViewById<TextView>(R.id.txt_nombre_calle)
        val txtNroCalle = findViewById<TextView>(R.id.txt_nro_calle)
        val btnConfirmar = findViewById<Button>(R.id.button_confirmar_comercio)
        val txtFechaSeleccionada = findViewById<TextView>(R.id.text_fecha_entrega)
        val spinnerHoraEntrega = findViewById<Spinner>(R.id.spinner_hora_entrega)
        val txtAlertaHora = findViewById<TextView>(R.id.textView_alerta_fechaHora)
        val lblSelecHora = findViewById<TextView>(R.id.textView_mostrar_hora)
        val lblSelecFecha = findViewById<TextView>(R.id.lbl_fecha_hora_entrega)
        val checkboxAntesPosible = findViewById<CheckBox>(R.id.checkBox_lo_antes_posible)
        val imgAlerta = findViewById<ImageView>(R.id.imgAlerta)


        val items = arrayOf("Córdoba", "Villa Allende", "Villa Carlos Paz", "San Francisco")
        val adapter: ArrayAdapter<String> =
            ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, items)
        spinnerCiudades.adapter = adapter

        spinnerCiudades.setSelection(items.indexOf(modelo.direccion_retiro_ciudad))
        spinnerCiudades.isEnabled = false



        txtNroCalle.inputType =
            InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED


        btnConfirmar.setOnClickListener {

            val calle = txtNombreCalle?.text.toString()
            val nro = txtNroCalle?.text.toString()
            val loAntesPosible = checkboxAntesPosible.isChecked
            val hsActual = spinnerHoraEntrega.selectedItem?.toString()

            if (!validarCampos(calle, nro, loAntesPosible, hsActual)) {
                Toast.makeText(this, "❌ Por favor complete todos los campos", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }
            modelo.direccion_entrega_numero_calle = txtNroCalle.text.toString()
            modelo.direccion_entrega_nombre_calle = txtNombreCalle.text.toString()
            modelo.direccion_entrega_ciudad = spinnerCiudades.selectedItem.toString()
            modelo.entrega_hora = spinnerHoraEntrega.selectedItem?.toString()
            modelo.direccion_entrega_lo_antes_posible = checkboxAntesPosible.isChecked

            val data = Intent()
            data.putExtra("model", modelo)
            setResult(RESULT_OK, data)
            finish()
        }


        checkboxAntesPosible.setOnClickListener {


            if (checkboxAntesPosible.isChecked) {
                lblSelecFecha.visibility = View.INVISIBLE
                txtFechaSeleccionada.visibility = View.INVISIBLE
                txtAlertaHora.visibility = View.VISIBLE
                txtAlertaHora.text = "Recibiras tu pedido lo antes posible"
                imgAlerta.visibility = View.VISIBLE
                lblSelecHora.visibility = View.INVISIBLE
                spinnerHoraEntrega.visibility = View.INVISIBLE
            } else {
                lblSelecFecha.visibility = View.VISIBLE
                txtFechaSeleccionada.visibility = View.VISIBLE
                lblSelecHora.visibility = View.VISIBLE
                spinnerHoraEntrega.visibility = View.VISIBLE
                imgAlerta.visibility = View.INVISIBLE
                txtAlertaHora.visibility = View.INVISIBLE

            }
        }


        txtFechaSeleccionada.setOnFocusChangeListener { _, hasfocus ->
            if (!hasfocus) return@setOnFocusChangeListener
            txtFechaSeleccionada.clearFocus()
            val date =
                DatePickerDialog.OnDateSetListener { view, year, month, day ->
                    val monthPlus = month + 1
                    fechaSeleccionadaEnVista[Calendar.YEAR] = year
                    fechaSeleccionadaEnVista[Calendar.MONTH] = month
                    fechaSeleccionadaEnVista[Calendar.DAY_OF_MONTH] = day
                    modelo.entrega_fecha = fechaSeleccionadaEnVista
                    txtFechaSeleccionada.text = "$day/$monthPlus/$year"
                }
            val datePicker = DatePickerDialog(
                this,
                date,
                fechaSeleccionadaEnVista[Calendar.YEAR],
                fechaSeleccionadaEnVista[Calendar.MONTH],
                fechaSeleccionadaEnVista[Calendar.DAY_OF_MONTH]
            )

            datePicker.datePicker.minDate = Calendar.getInstance().timeInMillis - 1000
            datePicker.datePicker.maxDate =
                Calendar.getInstance().timeInMillis + 1000 * 60 * 60 * 24 * 7
            datePicker.show()
        }



        txtFechaSeleccionada.addTextChangedListener {
            if (fechaSeleccionadaEnVista[Calendar.YEAR] != null) {
                spinnerHoraEntrega.visibility = View.VISIBLE
                lblSelecHora.visibility = View.VISIBLE
            } else {
                spinnerHoraEntrega.visibility = View.INVISIBLE
            }
            val currentDate = Calendar.getInstance()
            var minHourIndex = currentDate.get(Calendar.HOUR_OF_DAY) - 7
            if (minHourIndex < 0) minHourIndex = 0

            var arrayList = horas

            if (fechaSeleccionadaEnVista[Calendar.DAY_OF_YEAR] == currentDate.get(Calendar.DAY_OF_YEAR)) {
                arrayList = getSubArray(horas, minHourIndex, horas.size - 1)
            }

            val arrayAdapter: ArrayAdapter<String> =
                ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayList)
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerHoraEntrega.adapter = arrayAdapter


        }





        spinnerHoraEntrega.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                txtAlertaHora.visibility = View.VISIBLE

                val dia = fechaSeleccionadaEnVista[Calendar.DAY_OF_MONTH]
                val mes = fechaSeleccionadaEnVista[Calendar.MONTH]
                val year = fechaSeleccionadaEnVista[Calendar.YEAR]
                val hsSelected = spinnerHoraEntrega.selectedItem.toString()

                txtAlertaHora.visibility = View.VISIBLE
                imgAlerta.visibility = View.VISIBLE

                txtAlertaHora.text =
                    "Recibiras tu pedido el $dia/$mes/$year a las $hsSelected aprox."

            }


        }

    }


}

fun <T> getSubArray(array: Array<T>, beg: Int, end: Int): Array<T> {
    return array.copyOfRange(beg, end + 1)
}


fun validarCampos(
    txtCalle: String,
    nro: String,
    loAntesPosible: Boolean,
    hora: String?
): Boolean {
    return !txtCalle.isNullOrEmpty() && !nro.isNullOrEmpty() && (loAntesPosible || !hora.isNullOrEmpty())

}
