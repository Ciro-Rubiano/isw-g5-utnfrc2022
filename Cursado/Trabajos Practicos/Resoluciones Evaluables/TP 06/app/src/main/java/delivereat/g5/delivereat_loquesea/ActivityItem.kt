package delivereat.g5.delivereat_loquesea

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity


class ActivityItem : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item)
        title = "Datos de tu producto"

        //############ MODELO GENERAL ############
        val modelo = intent.extras!!.get("model") as DeliverEatModel

        //############ Views ############
        val btnConfirmar = findViewById<Button>(R.id.button_confirmar_item)
        val txtDescripcion = findViewById<TextView>(R.id.text_descripcion_producto)
        val txtMontoProducto = findViewById<TextView>(R.id.text_precio_producto)
        val switchEsPago = findViewById<Switch>(R.id.switch_hay_que_pagar)
        val imgView = findViewById<ImageView>(R.id.imageView_producto_seleccion)



        txtDescripcion.text = modelo.item_descripcion ?: ""
        if (modelo.item_precio != null && modelo.item_precio != 0) txtMontoProducto.text =
            modelo.item_precio?.toString()


        //############ RESULT FROM ACTIVITIES ############

        var resultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.data != null) //imagen seleccionada
                {
                    val selectedImage: Uri = result.data!!.data!!

                    val fileDescriptor =
                        applicationContext.contentResolver.openAssetFileDescriptor(
                            selectedImage,
                            "r"
                        )
                    val fileSize = fileDescriptor!!.length / 1024
                    Toast.makeText(this, "OK (" + fileSize.toString() + "KB)", Toast.LENGTH_SHORT)


                    if (fileSize <= 5 * 1024) {
                        imgView.setImageURI(selectedImage)
                        modelo.item_photo = selectedImage.toString()
                    } else {
                        Toast.makeText(
                            this,
                            "La imagen pesa mas de 5MB 😭 y no podemos procesarla. Por favor selecciona otra",
                            Toast.LENGTH_LONG
                        ).show()
                    }


                }
            }

        //############ EVENTS ############


        btnConfirmar.setOnClickListener {

            if (validarCampos(
                    txtDescripcion.text.toString(),
                    txtMontoProducto.text.toString(),
                    switchEsPago.isChecked
                )
            ) {

                modelo.item_descripcion =
                    txtDescripcion.text.toString().replace("\r", "").replace("\n", "")
                if (switchEsPago.isChecked) modelo.item_precio =
                    txtMontoProducto.text.toString().toInt()
                else modelo.item_precio = 0
                val responseIntent = Intent()
                responseIntent.putExtra("model", modelo)
                setResult(RESULT_OK, responseIntent)
                finish()

            }

        }

        switchEsPago.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                txtMontoProducto.visibility = View.VISIBLE
            } else {
                txtMontoProducto.visibility = View.INVISIBLE
            }
        }

        imgView.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            resultLauncher.launch(Intent.createChooser(intent, "Select Picture"))
        }


    }

    private fun validarCampos(
        txt_descripcion: String,
        txt_monto_producto: String,
        switch_es_pago: Boolean
    ): Boolean {
        if (txt_descripcion.isNullOrEmpty()) {
            return false
        }
        if (txt_monto_producto.isNullOrEmpty() && switch_es_pago) {
            return false
        }
        return true
    }
}