package delivereat.g5.delivereat_loquesea

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import java.util.*


class MainActivity : AppCompatActivity() {

    var modelo: DeliverEatModel = DeliverEatModel();
    val alphaDisabled: Float = 0.2f

    override fun onResume() {

        controlNavigationFlow(modelo)

        super.onResume()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main)


        setTitle("Pedir lo que sea! 😀")


        val btn_siguiente = findViewById<Button>(R.id.button_confirmar_pedido)

        val lbl_main_producto = findViewById<TextView>(R.id.lbl_descripcion_producto_main)
        val lbl_direccion_destino = findViewById<TextView>(R.id.lbl_destino)
        val lbl_direccion_recogida = findViewById<TextView>(R.id.lbl_recogida)
        val lbl_pago = findViewById<TextView>(R.id.lbl_pago)
        val imageView_subir_foto = findViewById<ImageView>(R.id.imageView_miniatura_producto)
        val imageView_icono_recogida = findViewById<ImageView>(R.id.icono_recogida)
        val imageView_icono_destino = findViewById<ImageView>(R.id.icono_destino)
        val imageView_icono_pago = findViewById<ImageView>(R.id.icono_pago)


        var resultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->

                if (result.resultCode == Activity.RESULT_OK) {
                    //No hay RequestCodes
                    val data: Intent? = result.data

                    //do something...

                    modelo =
                        data?.extras?.get("model") as DeliverEatModel //colocamos nuevamente el modelo recuperado de los eventos de actividades hijas


                    lbl_main_producto.text =
                        modelo.item_descripcion?.take(30) + "... ($" + modelo.item_precio.toString() + ")"
                    if (modelo.direccion_entrega_nombre_calle != null && modelo.direccion_entrega_numero_calle != null) {
                        lbl_direccion_destino.text =
                            modelo.direccion_entrega_nombre_calle + " al " + modelo.direccion_entrega_numero_calle

                        if (modelo.direccion_entrega_lo_antes_posible!!) {
                            //lo antes posible
                            lbl_direccion_destino.text =
                                lbl_direccion_destino.text.toString() + "\nLo antes posible"
                        } else {
                            val date: Calendar = modelo.entrega_fecha!!
                            val dateFormated: String =
                                "${date.get(Calendar.DAY_OF_MONTH)}/${date.get(Calendar.MONTH) + 1}/${
                                    date.get(Calendar.YEAR)
                                }"
                            lbl_direccion_destino.text =
                                lbl_direccion_destino.text.toString() + "\n(" + dateFormated + " - ${modelo.entrega_hora})"
                        }
                    }

                    if (modelo.direccion_retiro_nombre_calle != null && modelo.direccion_retiro_numero_calle != null) {
                        lbl_direccion_recogida.text =
                            modelo.direccion_retiro_nombre_calle + " al " + modelo.direccion_retiro_numero_calle
                    }
                    this.resolveImage(modelo)
                    //TODO: Cargar datos de las tarjetas y los montos
                    if (modelo.pago_monto_efectivo != null || modelo.pago_tarjeta_cvc != null) {
                        if (modelo.pago_con_efectivo!!) {
                            lbl_pago.text = "Pagas con efectivo ($${modelo.pago_monto_efectivo})"
                        } else {
                            lbl_pago.text = "Pagas con tu Master terminada en ${
                                modelo.pago_tarjeta_numero!!.substring(12)
                            }"
                            lbl_pago.textSize = 15f
                        }
                    }

                }
            }

        imageView_subir_foto.setOnClickListener()
        {
            val intent = Intent(this, ActivityItem::class.java)
            intent.putExtra("model", modelo)
            resultLauncher.launch(intent)
        }

        lbl_pago.setOnClickListener() {
            val intent = Intent(this, ActivityPago::class.java)
            intent.putExtra("model", modelo)
            resultLauncher.launch(intent)

        }

        btn_siguiente.setOnClickListener()
        {
            Toast.makeText(
                this,
                "✅ Confirmamos tu pedido, lo recibirás cuando lo pediste",
                Toast.LENGTH_LONG
            ).show()
        }

        lbl_direccion_destino.setOnClickListener()
        {
            val intent = Intent(this, ActivityDestino::class.java)
            intent.putExtra("model", modelo)
            resultLauncher.launch(intent)

        }



        lbl_main_producto.setOnClickListener() {
            val intent = Intent(this, ActivityItem::class.java)
            intent.putExtra("model", modelo)
            resultLauncher.launch(intent)
        }


        imageView_icono_destino.setOnClickListener()
        {
            val intentChange = Intent(this, ActivityDestino::class.java);
            startActivity(intentChange);
        }

        lbl_direccion_recogida.setOnClickListener()
        {
            val intentChange = Intent(this, ActivityRetiro::class.java);
            intentChange.putExtra("model", modelo)
            resultLauncher.launch(intentChange)

        }

        imageView_icono_recogida.setOnClickListener()
        {
            val intentChange = Intent(this, ActivityRetiro::class.java);
            startActivity(intentChange);
        }


    }

    private fun resolveImage(modelo: DeliverEatModel) {
        if (modelo.item_photo == null) return
        var selectedImage: Uri? = Uri.parse(modelo.item_photo);

        val imageBox = findViewById<ImageView>(R.id.imageView_miniatura_producto)
        imageBox.setImageURI(selectedImage)

    }

    private fun controlNavigationFlow(modelo: DeliverEatModel) {
        //VALIDACION DE AVANCE DE TRAMITE

        //Etapa2
        val lbl_direccion_recogida = findViewById<TextView>(R.id.lbl_recogida)
        val imageView_icono_recogida = findViewById<ImageView>(R.id.icono_recogida)
        //Etapa3
        val lbl_direccion_destino = findViewById<TextView>(R.id.lbl_destino)
        val imageView_icono_destino = findViewById<ImageView>(R.id.icono_destino)
        //Etapa4
        val lbl_pago = findViewById<TextView>(R.id.lbl_pago)
        val imageView_icono_pago = findViewById<ImageView>(R.id.icono_pago)
        //Confirmacion
        val buttonConfirmar = findViewById<Button>(R.id.button_confirmar_pedido)

        if (modelo.item_descripcion != null) {
            //habilitar Retiro
            lbl_direccion_recogida.alpha = 1f
            lbl_direccion_recogida.isEnabled = true
            imageView_icono_recogida.isEnabled = true
            imageView_icono_recogida.alpha = 1f
        } else {
            //desabilitar Retiro
            lbl_direccion_recogida.alpha = alphaDisabled
            lbl_direccion_recogida.isEnabled = false
            imageView_icono_recogida.isEnabled = false
            imageView_icono_recogida.alpha = alphaDisabled

        }
        if (modelo.direccion_retiro_nombre_calle != null) {
            //habilitar Entrega
            lbl_direccion_destino.alpha = 1f
            lbl_direccion_destino.isEnabled = true
            imageView_icono_destino.isEnabled = true
            imageView_icono_destino.alpha = 1f
        } else {
            //desabilitar entrega
            lbl_direccion_destino.alpha = alphaDisabled
            lbl_direccion_destino.isEnabled = false
            imageView_icono_destino.isEnabled = false
            imageView_icono_destino.alpha = alphaDisabled

        }
        if (modelo.direccion_entrega_nombre_calle != null) {
            //habilitar tarjetas
            lbl_pago.alpha = 1f
            lbl_pago.isEnabled = true
            imageView_icono_pago.alpha = 1f
            imageView_icono_pago.isEnabled = true
        } else {
            lbl_pago.alpha = alphaDisabled
            lbl_pago.isEnabled = false
            imageView_icono_pago.alpha = alphaDisabled
            imageView_icono_pago.isEnabled = false
        }


        buttonConfirmar.isEnabled =
            modelo.pago_tarjeta_numero != null || modelo.pago_monto_efectivo != null


        //###############################
    }


}


