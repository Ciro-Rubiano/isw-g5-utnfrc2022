package delivereat.g5.delivereat_loquesea

import java.io.Serializable
import java.util.*

data class DeliverEatModel(
    var direccion_retiro_nombre_calle : String?,
    var direccion_retiro_numero_calle : String?,
    var direccion_retiro_ciudad : String?,
    var direccion_retiro_aclaracion : String?,
    var direccion_entrega_nombre_calle : String?,
    var direccion_entrega_numero_calle : String?,
    var direccion_entrega_ciudad : String?,
    var direccion_entrega_aclaracion : String?,
    var direccion_entrega_lo_antes_posible : Boolean?,
    var entrega_fecha : Calendar?,
    var entrega_hora : String?,
    var pago_con_efectivo : Boolean?,
    var pago_tarjeta_numero : String?,
    var pago_tarjeta_titular : String?,
    var pago_tarjeta_vencimiento :String?,
    var pago_total : Number?,
    var pago_monto_efectivo: Number?,
    var pago_tarjeta_cvc : String?,
    var item_descripcion : String?,
    var item_photo : String?,
    var item_precio : Number?


    ) : Serializable
{
    constructor():this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        0,
    )


    override fun toString(): String {
        return "DeliverEatModel(direccion_retiro_nombre_calle='$direccion_retiro_nombre_calle', direccion_retiro_numero_calle='$direccion_retiro_numero_calle', direccion_retiro_ciudad='$direccion_retiro_ciudad', direccion_retiro_aclaracion='$direccion_retiro_aclaracion', direccion_entrega_nombre_calle='$direccion_entrega_nombre_calle', direccion_entrega_numero_calle='$direccion_entrega_numero_calle', direccion_entrega_ciudad='$direccion_entrega_ciudad', direccion_entrega_aclaracion='$direccion_entrega_aclaracion', pago_tarjeta=$pago_con_efectivo, pago_tarjeta_numero='$pago_tarjeta_numero', pago_tarjeta_titular='$pago_tarjeta_titular', pago_total=$pago_total, pago_paga_con_efectivo=$pago_monto_efectivo, pago_tarjeta_cvc='$pago_tarjeta_cvc', item_descripcion='$item_descripcion', item_photo=$item_photo, item_precio=$item_precio)"
    }


}