package delivereat.g5.delivereat_loquesea

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.widget.addTextChangedListener
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import kotlin.random.Random

class ActivityPago : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pago)

        val modelo = intent.extras!!.get("model") as DeliverEatModel;

        val buttonConfirmar = findViewById<Button>(R.id.buttonConfirmar)
        val txtTarjeta = findViewById<TextView>(R.id.txtTarjeta)
        val txtVencimiento = findViewById<TextView>(R.id.txtVencimiento)
        val txtCVC = findViewById<TextView>(R.id.txtCVC)
        val txtPagoEnEfectivo = findViewById<TextView>(R.id.txtPagoEnEfectivo)
        val switchEfectivo = findViewById<Switch>(R.id.switchEfectivo)
        val txtTitular = findViewById<TextView>(R.id.txtTitular)
        val txtDescripcionProducto = findViewById<TextView>(R.id.txt_descripcion_producto)
        val lblTarjetaInvalida = findViewById<TextView>(R.id.lblTarjetaInvalida)
        val txtMontoproducto = findViewById<TextView>(R.id.txt_montoProducto)
        val txtMontoenvio = findViewById<TextView>(R.id.txt_montoEnvio)
        val txtMontototal = findViewById<TextView>(R.id.txt_montoTotal)
        val cardTapadoTarjeta = findViewById<CardView>(R.id.card_tapadoTarjeta)


        cardTapadoTarjeta.visibility = View.GONE


        buttonConfirmar.setOnClickListener() {


            txtVencimiento.clearFocus()

            var flagAllOk : Boolean = true
            var listOfViews : List<TextView>
            var flagTarjeta : Boolean = false
            if(!switchEfectivo.isChecked){
                listOfViews = listOf(txtVencimiento, txtCVC, txtTitular, txtTarjeta)


            }else{
                listOfViews = listOf(txtPagoEnEfectivo)

            }



            for( v: TextView in listOfViews){
                flagAllOk = flagAllOk && !v.text.isNullOrEmpty()
            }

            if (!flagAllOk)
            {
                Toast.makeText(this, "❌ Por favor complete todos los campos", Toast.LENGTH_LONG).show()
            }
            else{

                if (lblTarjetaInvalida.visibility == View.VISIBLE)
                {
                    return@setOnClickListener
                }

                if(switchEfectivo.isChecked && modelo.pago_total!!.toInt() > txtPagoEnEfectivo.text.toString().toInt()){
                    Toast.makeText(this, "❌ El monto de pago en efectivo es menor al total", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }

                if(flagAllOk){
                    //return mainActivity

                    modelo.pago_tarjeta_numero = txtTarjeta.text.toString()
                    modelo.pago_tarjeta_cvc = txtCVC.text.toString()
                    modelo.pago_tarjeta_vencimiento = txtVencimiento.text.toString()
                    modelo.pago_tarjeta_titular = txtTitular.text.toString()
                    modelo.pago_con_efectivo = switchEfectivo.isChecked
                    if(!txtPagoEnEfectivo.text.toString().isNullOrEmpty()) modelo.pago_monto_efectivo = txtPagoEnEfectivo.text?.toString()?.toInt()

                    val responseIntent = Intent()
                    responseIntent.putExtra("model",modelo)
                    setResult(RESULT_OK, responseIntent)
                    finish()
                }
                else{
                    Toast.makeText(this, "❌ Por favor, completá todos los campos", Toast.LENGTH_LONG).show()
                }


            }


        }



        val montoEnvio = Random.nextInt(100, 700)
        txtMontoproducto.text = "$"+modelo.item_precio.toString()
        txtMontoenvio.text = "$"+montoEnvio.toString()
        txtMontototal.text = "$"+(modelo.item_precio!!.toInt() + montoEnvio)
        txtDescripcionProducto.text = modelo.item_descripcion!!.take(15)
        modelo.pago_total = modelo.item_precio!!.toInt() + montoEnvio


        txtVencimiento.onFocusChangeListener = View.OnFocusChangeListener(){view, gain ->

            if(!gain){
                val text = txtVencimiento.text.toString()
                try{

                    val localDate = LocalDate.parse("01/"+text, DateTimeFormatter.ofPattern("dd/MM/yy"))
                    if(!localDate.isAfter(LocalDate.now())){
                    //antigua
                    Toast.makeText(this, "La tarjeta está caducada", Toast.LENGTH_LONG).show()
                    txtVencimiento.text = ""
                    }

                }catch (_:Exception){
                    Toast.makeText(this, "La fecha de vencimiento es invalida", Toast.LENGTH_LONG).show()
                    txtVencimiento.text = ""
                }

            }

        }

        txtTarjeta.addTextChangedListener() {
            if ((txtTarjeta.text.length != 16) || (txtTarjeta.text[0].toString() != "5")) {
                lblTarjetaInvalida.visibility = View.VISIBLE
                //buttonConfirmar.isEnabled = false

            } else {
                lblTarjetaInvalida.visibility = View.INVISIBLE
                //buttonConfirmar.isEnabled = true

            }
        }

        switchEfectivo.setOnCheckedChangeListener(){ _, isChecked ->
            if (isChecked) {
                txtPagoEnEfectivo.visibility = View.VISIBLE;
                cardTapadoTarjeta.visibility = View.VISIBLE
            } else {
                txtPagoEnEfectivo.visibility = View.INVISIBLE
                cardTapadoTarjeta.visibility = View.GONE
            }
        }

        //TODO: Validar que el monto de efectivo sea mayor al precio total


    }
}

